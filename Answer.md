1. 
a. Strings in Rust 
letter = "A" | "B" | "C" | "D" | "E" | "F" | "G"
       | "H" | "I" | "J" | "K" | "L" | "M" | "N"
       | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
       | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
       | "c" | "d" | "e" | "f" | "g" | "h" | "i"
       | "j" | "k" | "l" | "m" | "n" | "o" | "p"
       | "q" | "r" | "s" | "t" | "u" | "v" | "w"
       | "x" | "y" | "z" ;
digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;
symbol = "[" | "]" | "{" | "}" | "(" | ")" | "<" | ">"
       | "'" | '"' | "=" | "|" | "." | "," | ";" | "\"
       | "/" | "!" | "@" | "#" | "~" | "$" | "%" | "^"
       | "&" | "*" | "?" | "`" | "-" | "+" | "_" ;
Ws = " " | "\n";
all_character = letter | digit | symbol | ws ;
string = '"', {(all_character - '"' - "\")| '\"'| "\\" }, '"';

b. Comments in Rust

Comment = ("//",{all_character - "\n"}) | ("/*", {all_character - "\n"}, "*/");

c. E-mail Addresses

Name = {all_character - ws};
Company = {all_character - ws};
Organization = letter, [letter], [letter];
Country = {all_character - ws};
e_mail = name, "@", company, ".", organization, ["." , country]


d. Phone Number
N = (digit - "0" - "1");
NPA = digit, digit, digit;
NXX = N, digit , digit;
XXXX = digit, digit, digit, digit;
phone_number = NPA, "-", NXX, "-", XXXX;

e. Numeric Constants in Rust
binary = "0b", {"0"| "1"};
octal = "0o", {digit - "8" - "9"};
decimal = ["-"], (digit - "0"), [{digit}], [".", {digit}] | "0";
hexadecimal = "0x", {digit | "A" | "B" | "C" | "D" | "E" | "F"}; 
scientific = ["-"], (digit - "0"), ".", {digit}, "e", ["-"], {digit};
constants_in_rust = binary | octal | decimal | hexadecimal | scientific;

2.
a) Lexical Error : sting str = "abc"; // String was spelled wrong

b) Syntax Error : println!("Hello World); // Miss " at the end of the string

c) Static Semantic Error : " use of an identifier that was never declared"

d) Ownership Error:
fn main() {
    let s = String::from("hello");
    change(&s);
}

fn change(some_string: &String) {
    some_string.push_str(", world");
}

e) Lifetime Error: 
Fn main(){
    let r;
    {
        let x = 5;
        r = &x;
    }
    println!("r: {}", r);
}

3.
the isize and usize types depend on the kind of computer your program is running on: 64 bits if you’re on a 64-bit architecture and 32 bits if you’re on a 32-bit architecture. when overflow happen, the values greater than the maximum value the type can hold “wrap around” to the minimum of the values the type can hold

4.
its difficult to tell whether a program is correct or not is because human and the machine work differently. What we had in mind sometimes is not what the machine reads. I personally find bugs by compile the program and wait for the error message to pop up. If not, ill run it and see if there is any runtime error. If both do not have any error but the output is wrong I like to add println in to my code to see what the result after each step and narrow it down by adding more println.
The bugs cannot be found: 
Perform related error

The bugs can be found:
Data table
Undeclared variable
 


5.
A.
print(x);
statement
  |subroutine_call
    |identifier
      |"print"
    |"("
    |argument_list
      |expression
        |primary
          |identifier
            |"x"
        |expression_tail
          |e
      |argument_tail
        |e
    |")"

B.
y := add(a,b)
statement
  |assignment
    |identifier
      |y
    |:=
    |expression
      |primary
        |subroutine_call
          |identifier
            |add
          |"("
          |argument_list
            |expression
              |primary
                |identifier
                  |a
              |expression_tail
                |e
            |argument_tail
              |,
              |argument_list
                |expression
                  |primary
                    |identifier
                      |b
                  |expression_tail
                    |e
                |argument_tail
                  |e
          |")"
      |expression_tail
        |e

C.
z := 1 + 2 * 3
statement
  |assignment
    |identifier
      |z
    |:=
    |expression
      |primary
        |identifier
          |1
      |expression_tail
        |operator
          |+
        |expression
          |primary
            |identifier
              |2
          |expression_tail
            |operator
              |*
            |expression 
              |primary
                |identifier
                  |3
              |expression_tail
                |e

D.
x := 9 * sin(x)
statement
  |assignment
    |identifier
      |"x"
    |":="
    |expression
      |primary
        |identifier
          |"9"
      |expression_tail
        |operator
          |"*"
        |expression
          |primary
            |subroutine_call
              |identifier
                |"sin"
              |"("
              |argument_list
                |expression
                  |primary
                    |identifier
                      |"x"
                  |expression_tail
                    |e
                |argument_tail
                  |e
              |")"
          |expression_tail
            |e

6.
A. 
G can be grammar balanced or grammar unbalanced if decide to use balanced () it will not get into unbalanced but if you choose unbalanced (] in the beginning you can get back to balanced 

B.
((]()
G
  |G
    |G
      |e
    |N
      |"("
      |L
        |L
          |e
        |"("
      |"]"
  |B
    |"("
    |E
      |e      
    |")"


C. 
((()))
G
  |G
    |e
  |B
    |"("
    |E
      |E
        |e
      |"("
      |E
        |E
          |e
        |"("
        |E
          |e
        |")"
      |")"
    |")"














